#include <ctime>

void coinc() {
    time_t begin = time(NULL);
    // Load file and coresponding trees
    TFile * f = new TFile("fibres.root","READ");
    TTree * tree = (TTree*)f->Get("MyTree");
    Float_t threshold = 0.067;

    // Initialize storage of the coincidence data
    Int_t coinc[4][4][4][4];
    TH2F * hcoinc[4][4];
    for(Int_t i=0;i<4;i++){
        for(Int_t j=0;j<4;j++){
            hcoinc[i][j] = new TH2F(Form("hcoinc_%i_%i",i,j),Form("Board %i, Channel %i",i,j),4,-0.5,3.5,4,-0.5,3.5);
        }
    }

    char * cutString;
    // Use the Draw() function to get the number of events passing the cut condition (two or more photons)
    for(Int_t board=0;board<4;board++){
        for(Int_t channel=0;channel<4;channel++){
            // calculat first normalization
            cutString = Form("amplitudes_%i_%i",board,channel);
            tree->Draw(Form(Form("amplitudes_%i_%i",board,channel)),cutString);
            TH1F * htemp = gROOT->FindObject("htemp");
            coinc[board][channel][board][channel] = htemp->GetEntries();
            // loop through all combinations
            for(Int_t coinc_board=0;coinc_board<4;coinc_board++){
                for(Int_t coinc_channel=0;coinc_channel<4;coinc_channel++){
                    if(board != coinc_board || channel != coinc_channel) {
                        cutString = Form("amplitudes_%i_%i>%f&&amplitudes_%i_%i>%f",board,channel,threshold,coinc_board,coinc_channel,threshold);
                        tree->Draw(Form(Form("amplitudes_%i_%i",board,channel)),cutString);
                        TH1F * htemp = gROOT->FindObject("htemp");
                        coinc[board][channel][coinc_board][coinc_channel] = htemp->GetEntries();
                        //cout << board*64+channel*16+coinc_board*4+coinc_channel+1 << "/256\t" << cutString << "\t" <<  coinc[board][channel][coinc_board][coinc_channel] << endl;
                    }
                    hcoinc[board][channel]->SetBinContent(coinc_board+1,coinc_channel+1,coinc[board][channel][coinc_board][coinc_channel]);
                }
            }
            // Add axes labels
            hcoinc[board][channel]->GetXaxis()->SetTitle("board");
            hcoinc[board][channel]->GetYaxis()->SetTitle("channel");
        }
    }
    
    // Save the resulting histograms in a pdf file
    TCanvas * c1 = new TCanvas("c1");
    c1->Divide(4,4);
    for(Int_t board=0;board<4;board++){
        for(Int_t channel=0;channel<4;channel++){
            c1->cd(board*4+channel+1);
            gStyle->SetOptStat(0);
            hcoinc[board][channel]->Draw("zcol");
        }
    }
    c1->SaveAs("coinc_macro.pdf");
    time_t end = time(NULL);
    std::cout << "Used time: " << float(end - begin) << "s" << std::endl;
}

From Binary to Wafevorms
========================

Goal: read the binary file **run.dat** and look at the stored waveforms
www.phys.ethz.ch/~corrodis/run.dat

Questions and Tasks
-------------------
* create a branch with your name and add your scripts to the repository
* what is the run number? When was the file generated?
* look at some waveforms from all channels (write a viewer)
* what is the noise level? Mean and sigma of the distribution?
* write a script which determines:
  * the rise time of the signals
  * the charge from the PMTs (integral)
* what is the time distribution of the digital pulses?
* what is the time distribution of the PMT pulses? 
* what is the energy/charge distribution?
* inter-calibrate the 7 calo PMTs and try to sum that. Is there something hidden? ;-)

Binary Format
-------------
* file header
  * 32 bit: unixtime
  * 32 bit: run number
  * 16 bit: waveform length (l)
  *  8 bit: sampling rate in MHz
  * 16 bit: pre-trigger
  *  8 bit: number of channels (nch)
  *  8 bit: reserved for flags
  *  8 bit: magic number (42)
* event header
  * 16 bit: 0xCAFE (header)
  * 32 bit: event number board 0
  * 32 bit: event number board 1
* data
  * 16 bit x l x nch: dac values 
* file trailer
  * 16 bit: 0xBABE
  * 31 bit: number of events

Data Format/Channel Map
-----------------------
* channel 0: noise
* channel 1 & 2: digital pulse
* channel 3 - 9: PMT from calorimeter 
